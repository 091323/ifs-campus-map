# Campus Map with leaflet

## Link to [Campus Map](https://geometalab.gitlab.io/campus-maps-with-openstreetmap/ifs-campus-map/index.html)

## Start
Ich habe das Projekt etwas umgebaut, sodass ihr normal Arbeiten könnt. Um starten zu können müsst ihr [node](https://nodejs.org/en/) auf euren Rechner installiert haben. Wenn ihr Windows benutzt (von dem ich abrate), solltet ihr euch evt. mit dem [git bash](https://gitforwindows.org/) vertraut machen. 

Wenn ihr alles installiert habt, installiert mit dem Befehl die sogenannten `node modules`.

```bash
npm install
```
Was der Befehl macht: Er geht in die Datei `package.json` und schaut dort nach *dependencies* und *devDependencies* und installiert diese in einem Ordner welcher `node_modules` heisst. Der Ordner `node_modules` wird extrem gross sein. Das ist aber nicht euer Problem und ihr könnt ihn eigentlich komplett ignorieren (git ignoriert ihn auch). 

## Befehle 

### npm run dev
In der Entwicklung ist es praktisch, wenn man nicht jedesmal die Seite manuel neu laden muss. Deswegen gibt es den Befehl: `npm run dev`. Mit diesem Befehl wird automatisch beim Speichern eure Änderungen auf dem Browser angepasst.

### npm run build
In einer live Umgebung möchte man das Javascript so effizient wie möglich ausliefern. Daher wird mit dem Befehl `npm run build` die Dateien im `src/`-Ordner genommen, zusammen kompiliert und schlussendlich als `main.js` in den Ordner `dist` gelegt. Unsere `index.html`-Datei referenziert jetzt nicht mehr auf das orginal script sondern auf das neue `main.js` Script. 

Probier es selber aus: 
1. Lass `npm run build` laufen
2. Schau im `dist/index.html` auf die Zeile 52.
3. Gehe in das File `dist/main.js` und vergleiche es mit deinem Source Code im Ordner `src`. Siehst du Ähnlichkeiten mit deinem Orginalen Code? Nein -> Gut, weil das Javascript wurde optimiert und "production-ready" gemacht. 

## Ordnerstruktur
- **dist** Beinhaltet das auszuliefernde Produkt. Wenn ihr also euer Projekt ausliefern möchtet, müsst ihr die Anfragen auf die index.html Datei lenken.
- **src** Ist euer Javascript Code. Der Einstiegspunkt ist hier `index.js`. 
- **package-lock.json** Könnt ihr ignorieren. Das ist eine Hilfdatei für das NPM package management.
- **package.json** Hier werden Abhängigkeiten und die beiden Commands `npm run dev` und `npm run build` definiert.
- **webpack.config.js** Das ist ein Konfigurationsfile für Webpack. Schaut mal rein. 

## Was ich euch empfehlen kann zum lernen:
- NPM Tutorial, [NPM](https://www.youtube.com/watch?v=2V1UUhBJ62Y)
- Webpack basic tutorial, [WEBPACK](https://webpack.js.org/guides/getting-started/#basic-setup)

## Kleine Herausforderung
- Die Existenz von `index.html` ist mir ein Dorn in Auge. Da wir das viel einfacher dynamisch erstellen können. Versucht Webpack so zu konfigurieren, dass ihr `dist/index.html` nicht mehr Benötigt: 

Hinweis: 
- [HtmlWebpackPlugin](https://webpack.js.org/guides/output-management/#setting-up-htmlwebpackplugin)


## Was ist Webpack?
Es nimmt alle Resourcen (Bilder, CSS, JS, etc.) und kompiliert sie. Somit werden die Webseiten schneller, weil weniger Code beim Aufruf der Webseite geladen werden muss. Zusätzlich hat es noch weitere Features, wie hot module reloading. Es lohnt sich jedenfalls die Doku von [Webpack](https://webpack.js.org/) zu lesen.