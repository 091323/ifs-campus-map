import {getState} from './state'
import { MY_MAP } from './state/constants'
import {clearSearch} from './search'
import { toggleTileServer } from './map'

export const createMapControls = () => {
    const myMap = getState(MY_MAP)

    const plusBtn = document.querySelector('#plus')
    const minusBtn = document.querySelector('#minus')
    const resetBtn = document.querySelector('#reset')
    const changeMapBtn = document.querySelector('#changeMap')

    plusBtn.addEventListener('click', () => myMap.zoomIn())
    minusBtn.addEventListener('click', () => myMap.zoomOut())
    resetBtn.addEventListener('click', () => {
        myMap.setView([47.223345, 8.817153], 18);
        clearSearch()
    })
    changeMapBtn.addEventListener('click', () => toggleTileServer())
    
}