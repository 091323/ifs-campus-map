import { createSearch } from './search.js';
import { loadAllData } from './data.js';
import { getParameter, autoselectSearchParameter, removeUrlParams } from './helper.js';
import { createMyMap } from './map.js';
import { createLayers } from './layer.js';
import {updateState } from './state'
import {SELECTED_ITEM_IN_LIST, GEOJSON_DATA } from './state/constants'
import { createLevels } from './levels.js';
import {createMapControls} from './map-controls'

// init state
updateState(SELECTED_ITEM_IN_LIST, 0)
updateState(GEOJSON_DATA, null)



// the order is strictly defined
const setupApp = () => {
    createMyMap()
    createSearch()
    createLevels()
    createLayers()
    createMapControls()
}


loadAllData().then(data => {
    updateState(GEOJSON_DATA, data);
}).then(setupApp)
.then(() => {
    if(getParameter("search")){
        autoselectSearchParameter();
    }
});


//setup
window.onload = _ => {
    const input = document.getElementById("searchInput");
    input.focus();
};

// Function to create a link to share the map
window.share = id => {
    let address = window.location.href;
    address = removeUrlParams(["lat", "lon", "zoom", "search"], address)
    if(address.endsWith("/") || address.endsWith('index.html')) {
        address += '?search=' + id;
    } else {
        address += '&search=' + id;
    }

    // Change Url to address
    window.history.pushState("", "", address);

    // Save to clipboard
    navigator.clipboard.writeText(address).then(function() {
        // "Saved to clipboard" Popup when successful
        let popup = document.getElementById("sharePopup");
        let popupDiv = document.getElementById("sharePopupDiv");
        popup.classList.toggle("showPopup");
        setTimeout(function () { // After 3000ms, toggle popup off again
            popup.classList.toggle("showPopup");
        }, 3000)
    }, function(err) {
        console.error('Async: Could not copy text: ', err);
    });
}

