import { dataOnEachLevel, propertiesToIgnore } from './const.js';

export const loadAllData = async _ => {
    const allData = await getData();

    dataOnEachLevel['levelTwo'] = loadLevel(allData, '2');
    dataOnEachLevel['levelOne'] = loadLevel(allData, '1');
    dataOnEachLevel['levelZero'] = loadLevel(allData, '0');
    dataOnEachLevel['levelMinusOne'] = loadLevel(allData, '-1');

    dataOnEachLevel['levelCoridorTwo'] = loadLevelCorridors(allData, '2');
    dataOnEachLevel['levelCoridorOne'] = loadLevelCorridors(allData, '1');
    dataOnEachLevel['levelCoridorZero'] = loadLevelCorridors(allData, '0');
    dataOnEachLevel['levelCoridorMinusOne'] = loadLevelCorridors(allData, '-1');

    dataOnEachLevel['POI'] = loadPOIs(allData);

    return dataOnEachLevel;
};

const getData = async _ => {
    return fetch('data/ost.geojson')
            .then(response => response.json());
};

const loadLevel = (allData, levelToLoad) => {
    let results = [];

    for (let featureId in allData['features']) {
        const feature = allData.features[featureId];

        if (feature['properties']['level'] !== undefined) {
            const level = feature['properties']['level'];
            const splitLevels = level.split(";");
            const indoor = feature['properties']['indoor'];
            const room = feature['properties']['room'];
            if (splitLevels.includes(levelToLoad) && indoor !== 'corridor' && indoor !== 'area') {
                results.push(feature);
            } else if (splitLevels.includes(levelToLoad) && indoor === 'area' && room !== undefined) {
                results.push(feature);
            } else if (splitLevels.includes(levelToLoad) && feature['properties']['amenity'] !== undefined) {
                results.push(feature);
            }
        }
    }
    
    return results;
}

const loadLevelCorridors = (allData, levelToLoad) => {
    let results = [];

    for (let featureId in allData['features']) {
        const feature = allData.features[featureId];

        if (feature['properties']['level'] !== undefined) {
            const level = feature['properties']['level'];
            const splitLevels = level.split(";");
            const indoor = feature['properties']['indoor'];
            const room = feature['properties']['room'];
            if (splitLevels.includes(levelToLoad) && (indoor === 'corridor' || indoor === 'area') && room === undefined && feature['properties']['amenity'] === undefined) {
                results.push(feature);
            }
        }
    }
    
    return results;
}

const loadPOIs = allData => {
    let results = [];

    for (let featureId in allData['features']) {
        const feature = allData['features'][featureId];

        if(!(propertiesToIgnore.every(value => feature['properties'][value] === undefined))){
            continue;
        }
        if(feature['geometry']['type'] !== "Point"){
            continue;
        }
        if(feature['properties']['entrance'] !== 'main' && feature['properties']['door'] !== undefined){
            continue;
        }

        // Removes Points that only contain a relation
        if (Object.keys(feature['properties']).length === 2 && feature['properties']['@relations'] !== undefined) {
            continue;        
        }

        results.push(feature);
    }
    return results;
}