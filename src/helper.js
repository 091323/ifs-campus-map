import { roomPropertiesToShow, langenscheidt } from './const.js';
import * as search from "./search"
import {flyToSearchResult} from "./search";
import {MY_MAP} from "./state/constants";
import {getState} from "./state";

export const getParameter = key => { 
    const address = window.location.search;
    const parameterList = new URLSearchParams(address);
    return parameterList.get(key);
} 

export const autoselectSearchParameter = _ => {
    search.searchFromQuery(getParameter("search"))
}

export const formatSidebarInfo = (feature, infoBox) => {
    let properties = feature['properties'];
    let presentImportantProperties = [];
    let infotext = '';
    for (let id in properties) {
        if (roomPropertiesToShow.includes(id)) {
            presentImportantProperties.push(id);
        }
    }

    if (presentImportantProperties.includes('ref')) {
        if (presentImportantProperties.includes('name') && !(properties['name'].includes(properties['ref']))) {
            infotext += '<p><b>' + properties['name'] + '</b></p>';   
        }

        // Raumbezeichnung und Raumnummer
        if (presentImportantProperties.includes('room') && presentImportantProperties.includes('ref')){
            infotext += '<p><b>' + translate(properties['room']) + ' ' + properties['ref'] + '</b></p>';
        } else if (presentImportantProperties.includes('description')) {
            if (properties['description'].startsWith('Room') && presentImportantProperties.includes('room')) {
                infotext += '<p><b>' + translate(properties['room']) + ' ' + properties['ref'] + '</b></p>';
            } else {
                infotext += '<p><b>' + properties['description'] + '</b></p>';
            }
        } else if (presentImportantProperties.includes('name')) {
            infotext += '<p>' + properties['name'] + '</p>';
        } else {
            infotext += '<p>' + properties['ref'] + '</p>';   
        }
    } else if (presentImportantProperties.includes('amenity')) {
        infotext += '<p>' + properties['name'] + '</p><p>' + translate(properties['amenity']) + '</p>'
    } else if (presentImportantProperties.includes('name')) {
        infotext += '<p>' + properties['name'] + '</p>'
    }
    if (infotext !== '' && properties['level'] !== undefined) {
        let levels = properties['level'];
        if (levels.length === 1 || (levels.length === 2 && levels.startsWith('-'))) {
            infotext += '<p>Stockwerk ' + properties['level'] + '</p>';
        } else {
            levels = levels.split(';');
            infotext += '<p>Stockwerke ' + levels[0] + ' - ' + levels[levels.length-1] + '</p>';
        }
    }

    if (presentImportantProperties.includes('amenity')) {
        infotext += '<p>' + translate(properties['amenity']) + '</p>';
    }
    if (presentImportantProperties.includes('website')) {
        infotext += '<p>Website: <a href="' + properties['website'] + '">' + properties['website'].replace(/^https?:\/\//i, "") +'</a></p>';
    }
    if (presentImportantProperties.includes('phone')) {
        infotext += '<p>Telefonnummer: ' + properties['phone'] + '</p>';
    }
    if (presentImportantProperties.includes('email')) {
        infotext += '<p>E-Mail: <a href="mailto:' + properties['email'] + '">' + properties['email'] + '</a></p>';
    }
    
    let lat;
    let lng;
    if (feature['geometry']['type'] === 'Polygon') {
        let cors = L.polygon(feature['geometry']['coordinates']).getBounds().getCenter();
        lat = cors['lat'];
        lng = cors['lng'];
    }
    else if (feature['geometry']['type'] === 'Point') {
        let cors = feature['geometry']['coordinates'];
        lat = cors[0];
        lng = cors[1];
    }

    if (infotext !== '') {
        infotext +=
            `<div id="linkBox"><p id="links"><a href="https://routing.osm.ch/?z=14&loc=${lng},${lat}">OSM.ch</a> | ` +
            `<a href="https://map.search.ch/${lng},${lat}">Search.ch</a> | ` +
            `<a href="https://maps.google.com/maps?q=loc:${lng},${lat}">GMaps</a></p>` +
            `<div id="shareButton" type="button" onclick="share('${feature['id']}')">` +
            `<div id="sharePopupDiv"><span id="sharePopup">Copied to clipboard</span></div><img id="shareButtonSVG" src="data/icons/share.svg" alt="share button"></button></div>`;
    }
    infoBox.innerHTML = infotext
    infoBox.addEventListener("click", function (event) {
        // If clicked element is an img (like the share button), don't fly to result
        if(event.target.tagName.toLowerCase() !== 'img') {
            flyToSearchResult(feature, getState(MY_MAP))
        }
    })
}

const translate = (s) => {
    return langenscheidt[s];
}

export const formatKey = (s) => {
    if (s === 'ref') return 'Raumnummer: '
    if (s === 'description') return 'Beschreibung: '
    if (s === 'phone') return 'Telefonnummer: '
    if (s === 'room') return 'Raum: '
    if (s === 'indoor') return ''
    if (s === 'website') return ''
    if (s === 'amenity') return ''
    if (s === 'name') return ''
    if (s === 'playground') return ''
    if (s === 'leisure') return ''
    return s.charAt(0).toUpperCase() + s.slice(1) + ': '
};

export const formatValue = (s) => {
    if (s === 'laboratory') return 'Labor'
    if (s === 'office') return 'Büro'
    if (s === 'hall') return 'Halle'
    if (s === 'stairs') return 'Treppen'
    if (s === 'corridor') return 'Korridor'
    if (s === 'conference') return 'Sitzngszimmer'
    if (s === 'class') return 'Klassenzimmer'
    if (s === 'car_sharing') return 'Carsharing'
    if (s === 'bicycle_parking') return 'Fahrradständer'
    if (s === 'springy') return 'Federwippe'
    if (s === 'swing') return 'Schaukel'
    if (s === 'climbingframe') return 'Klettergerüst'
    if (s === 'drinking_water') return 'Trinkwasserstelle'
    if (s === 'shower') return 'Dusche'
    if (s === 'firepit') return 'Feuerstelle'
    if (s === 'charging_station') return 'Aufladestation'
    if (s === 'lecture') return 'Hörsaal'
    if (s === 'boat_rental') return 'Bootsverleih'
    if (s === 'room') return ''
    if (s === 'area') return ''
    if (s === 'wall') return ''
    if (s.startsWith('http')) return '<a href="' + s + '">Website</a>'
    return s.charAt(0).toUpperCase() + s.slice(1)
};


// Removes the parameters given in the keys array when existing
export const removeUrlParams = (keys, sourceURL) => {
    let baseURL = sourceURL.split("?")[0]
    let paramString = sourceURL.split("?")[1]
    if(paramString === undefined) {
        return sourceURL
    }
    let params = paramString.split("&")
    let remainingParams = [];
    for(let i in params) {
        let found = false;
        for (let j in keys) {
            if((params[i].split("="))[0] === keys[j]) {
                found = true
            }
        }
        if(!found) {
            remainingParams.push(params[i])
        }
    }
    let remainingParamsString = "";
    if(remainingParams.length !== 0) {
        remainingParamsString += "?"
    }
    for (let i = 0; i < remainingParams.length; i++) {
        remainingParamsString += remainingParams[i]
        if(i < remainingParams.length - 1) {
            remainingParamsString += "&"
        }
    }
    return baseURL + remainingParamsString
}