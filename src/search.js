import {getState} from './state'
import {GEOJSON_DATA, MY_MAP} from './state/constants'
import {clearSelectedFeature, selectGeojson} from './layer'
import {selectLevelMinusOne, selectLevelOne, selectLevelTwo, selectLevelZero} from './levels'
import {formatSidebarInfo} from './helper'

const localStorage = {
    searchInput: null,
    cancelButton: null,
    info: null,
    listbox: null
}

const initLocalStorage = () => {
    localStorage.searchInput = document.querySelector('#searchInput')
    localStorage.cancelButton = document.querySelector('#cancelButton')
    localStorage.searchButton = document.querySelector('#searchButton')
    localStorage.info = document.getElementById('info');
    localStorage.listbox = document.querySelector('#listbox')
}

export const createSearch = () => {
    const myMap = getState(MY_MAP)
    const geoJsonData = getState(GEOJSON_DATA)
    initLocalStorage()

    localStorage.searchInput.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) { // For starting search on enter
            event.preventDefault();
            searchByWord(event.target.value.toLowerCase(), geoJsonData, myMap);
        } else {
            listSearchResults(geoJsonData, myMap) // On every new entered char, refresh results
        }
    });

    localStorage.listbox.addEventListener("mouseover", function () {
        highlightResult(-1) // On mouseover, remove marked result
    })

    document.addEventListener("keydown", function (event) {
        // For selecting search results with arrow up and down
        if(event.key === "ArrowDown") {
            selectNextResult()
        } else if (event.key === "ArrowUp") {
            selectPreviousResult()
        }
    })

    localStorage.searchButton.addEventListener('click', () => searchByWord(localStorage.searchInput.value.toLowerCase(), geoJsonData, myMap))
    localStorage.cancelButton.addEventListener('click', () => clearSearch())
}


// All of these selectedResult * 1  prevent JS from adding 'selectedResult' like a string
const selectNextResult = () => {
    const results = document.getElementsByClassName("search-result")
    const selectedResult = getSelectedResult() * 1;
    if(results.length < selectedResult + 2) { // If at last result, wrap back around to first result
        highlightResult(0);
    } else if(results.length > selectedResult + 1) {
        highlightResult(selectedResult + 1)
    }
}

const selectPreviousResult = () => {
    const selectedResult = getSelectedResult() * 1;
    if(selectedResult === 0) { // If at first result, wrap back around to last result
        const results = document.getElementsByClassName("search-result")
        highlightResult(results.length - 1);
    } else if(selectedResult > 0) {
        highlightResult(selectedResult - 1)
    }
}

const highlightResult = (resultNr) => {
    resultNr = resultNr * 1
    if(resultNr < 0) {
        document.activeElement.blur(); // No active element
    } else {
        document.querySelector(`.search-result:nth-child(${resultNr + 1})`).focus();
    }
}

const getSelectedResult = () => {
    const results = document.getElementsByClassName("search-result")
    let selectedProperty = -1;
    for(let i in results) {
        if(results[i] === document.activeElement) {
            selectedProperty = i;
        }
    }
    return selectedProperty;
}

export const searchFromQuery = (query) => {
    const myMap = getState(MY_MAP)
    const geoJsonData = getState(GEOJSON_DATA)
    initLocalStorage()

    searchByWord(query, geoJsonData, myMap)
}


const searchByWord = (searchWord, geoJsonData, myMap) => {
    if (searchWord.length < 3 && !(localStorage.searchInput.value.toLowerCase() === "wc")) { return }

    const features = getAllMatchingFeatures(searchWord, geoJsonData);
    highlightResults(features);

    if (features.length === 1) {
        formatSidebarInfo(features[0], localStorage.info.innerHTML)
        flyToSearchResult(features[0], myMap)
    }
}

export const getAllMatchingFeatures = (searchWord, geoJsonData) => {
    const results = [];
    if (searchWord === "") {
        return results;
    }
    let filter = searchWord.toLowerCase();

    //TODO: auslagern
    if (filter === 'wc' || filter === 'toilette' || filter === 'toiletten') {
        filter = "toilets";
    }

    for (let layerName in geoJsonData) {
        const features = geoJsonData[layerName];

        for (let featureIndex in features) {
            let feature = features[featureIndex];
            let propertyName = feature['properties']['name'] === undefined ? "" : feature['properties']['name'].toLowerCase();
            let propertyRef = feature['properties']['ref'] === undefined ? "" : feature['properties']['ref'].toLowerCase();
            let propertyAltName = feature['properties']['alt_name'] === undefined ? "" : feature['properties']['alt_name'].toLowerCase();
            let propertyAmenity = feature['properties']['amenity'] === undefined ? "" : feature['properties']['amenity'].toLowerCase();

            if (propertyRef.includes(filter) || propertyName.includes(filter) || propertyAltName.includes(filter) || feature['id'].includes(filter) || propertyAmenity.includes(filter)) {
                results.push(feature);
            }
        }
    }

    return results;
}

const highlightResults = (features) => {
    if (features.length > 0) {
        const level = features[0]['properties']['level'];
        if (level === '0') {
            selectLevelZero(document.querySelector('#selectLevelZero'));
        }
        else if (level === '1') {
            selectLevelOne(document.querySelector('#selectLevelOne'));
        }
        else if (level === '2') {
            selectLevelTwo(document.querySelector('#selectLevelTwo'));
        }
        else if (level === '-1') {
            selectLevelMinusOne(document.querySelector('#selectLevelMinusOne'));
        }
    }
    selectGeojson(features);
    localStorage.listbox.innerHTML = '';
}

const listSearchResults = (geoJsonData, myMap) => {
    if (localStorage.searchInput.value.length > 2 || localStorage.searchInput.value.toLowerCase() === "wc") {
        const matches = getAllMatchingFeatures(localStorage.searchInput.value, geoJsonData);
        localStorage.info.innerHTML = '';

        if (matches.length === 0) {
            localStorage.listbox.innerHTML = '<h3>Keine Treffer. <br> <a href="https://gitlab.com/geometalab/campus-maps-with-openstreetmap/ifs-campus-map/-/wikis/Hilfe">Hilfe</a> | <a href="mailto:rj-geometalab@ost.ch?subject=Feedback%20zur%20Suche%20in%20IFS%20Campus%20Map&body=Liebes%20IFS%20/%20Geometa%20Lab%20%0A%0AFeedback%20zur%20IFS%20Campis%20Map:%20Ich%20suchte%20nach%20' + localStorage.searchInput.value + '.%20%0A%0ALiebe%20Grüsse,">Suchanfrage mailen</a></h3>';
            return;
        }

        const sortedMatches = sortMatches(matches);
        let innerHTML = '<ul class="search-results">';
        innerHTML += sortedMatches
            .map((match, _) =>
                `<button tabindex="1" class="search-result" data-id=${match['id']}>${match['properties']['name']}</button>`)
            .join(' ')
        innerHTML += '</ul>';

        localStorage.listbox.innerHTML = innerHTML;

        const listedSearchResults = document.querySelectorAll('.search-result')
        listedSearchResults.forEach(item =>
            item.addEventListener('click', (elem) => searchByWord(elem.target.getAttribute('data-id'), geoJsonData, myMap))
        )

    } else {
        localStorage.listbox.innerHTML = '';
    }
}

export const flyToSearchResult = (result, myMap) => {
    let lat, lng, coords;
    if (result['geometry']['type'] === 'Polygon') {
        coords = L.polygon(result['geometry']['coordinates']).getBounds().getCenter()
        lat = coords['lat'];
        lng = coords['lng'];
    } else if (result['geometry']['type'] === 'Point') {
        coords = result['geometry']['coordinates'];
        lat = coords[0];
        lng = coords[1];
    }
    myMap.flyTo([lng, lat], 20, { animate: true, duration: 0.5 });
}

export const clearSearch = () => {
    const searchInput = document.querySelector('#searchInput')
    const info = document.getElementById('info');
    const listbox = document.getElementById('listbox');
    searchInput.value = "";
    info.innerHTML = '';
    listbox.innerHTML = '';
    clearSelectedFeature();
}

const sortMatches = (matches) => {
    let results;
    let strings = [];
    let ints = [];
    for (let x = 0; x < matches.length; x++) {
        if (matches[x]['properties']['name'] === undefined) {
            matches[x]['properties']['name'] = matches[x]['properties']['ref'];
            ints.push(matches[x]);
        } else {
            if (matches[x]['properties']['name'].match(/^\d/)) {
                ints.push(matches[x]);
            } else {
                strings.push(matches[x]);
            }
        }
    }

    ints.sort(function (a, b) {
        let nameA = a['properties']['name'];
        let nameB = b['properties']['name'];
        if (!nameA) {
            return 1;
        }
        if (!nameB) {
            return -1;
        }
        if (nameA.length < 6 && nameB.length < 6) {
            return nameA - nameB;
        } else if (nameA.length >= 6 && nameB.length >= 6) {
            if (nameA.slice(0, 4) === nameB.slice(0, 4)) {
                if (nameA.substring(nameA.length - 1) < nameB.substring(nameB.length - 1)) {
                    return -1;
                } else if (nameA.substring(nameA.length - 1) > nameB.substring(nameB.length - 1)) {
                    return 1;
                }
            } else {
                return nameA.slice(0, 4) - nameB.slice(0, 4);
            }
        } else if (nameA.length >= 6 && nameB.length < 6) {
            if (nameA.slice(0, 5) === nameB.slice(0, 5)) {
                return 1;
            } else {
                return nameA.slice(0, 4) - nameB.slice(0, 4);
            }
        } else if (nameA.length < 6 && nameB.length >= 6) {
            if (nameA.slice(0, 4) === nameB.slice(0, 4)) {
                return -1;
            } else {
                return nameA.slice(0, 4) - nameB.slice(0, 4);
            }
        }
    });

    strings.sort((a, b) => {
        if (a['properties']['name'] < b['properties']['name'])
            return -1;
        if (a['properties']['name'] > b['properties']['name'])
            return 1;
        return 0;
    });
    results = strings.concat(ints);

    return results;
}