export const dataOnEachLevel = {
    'levelTwo': {},
    'levelOne': {},
    'levelZero': {},
    'levelMinusOne': {},
    'POI': {},
}

export const propertiesToIgnore = [
    'natural',
    'highway',
    'emergency',
    'barrier',
    'man_made',
];

export const selectedFeatureStyle = {
    "color": "red",
    "weight": 5,
    "opacity": 1,
};

export const hoverFeatureStyle = {
    "color": "orange",
    "weight": 5,
    "opacity": 1
}

export const roomStyle = {
    "color": "#16264c",
    "weight": 0,
    "opacity": 0,
    "fillOpacity": 1
};

export const corridorStyle = {
    "color": "gray",
    "stroke": "true",
    "weight": 1,
    "opacity": 1,
    "fillOpacity": 0.5
};

export const roomPropertiesToShow = [
    'name',
    'amenity',
    'ref',
    'room',
    'description',
    'indoor',
    'phone',
    'website',
    'email'
];

export const poiPropertiesToShow = [
    'name',
    'amenity',
    'locality',
    'website',
    'toursim',
    'playground',
    'operator',
    'leisure'
];

export const langenscheidt = {
    'office': 'Büro',
    'class': 'Unterrichtszimmer',
    'room': 'Zimmer',
    'laboratory': 'Labor',
    'corridor': 'Korridor',
    'lounge': 'Lounge',
    'storage': 'Lagerraum',
    'toilets': 'Toilette',
    'seminar_room': 'Seminarzimmer',
    'canteen': 'Kantine',
    'restaurant': 'Restaurant',
    'stairs': 'Treppe',
    'lobby': 'Eingangshalle',
    'conference': 'Sitzungszimmer',
    'technical': 'Gebäudetechnik',
    'toilet': 'WC-Raum',
    'computer': 'Informatikraum',
    'lecture': 'Hörsaal',
    'hall': 'Halle',
    'copier': 'Kopierraum',
    'elevator': 'Lift',
    'parking': 'Parkhaus',
    'auditorium': 'Aula',
    'library': 'Bibliothek',
    'reception': 'Empfang',
    'showroom': 'Seminarraum'
};

//All icon constants
export const infoIcon = L.icon({
    iconUrl: 'data/icons/info_icon.svg',
    iconSize: [20, 20],
});

export const picnicTableIcon = L.icon({
    iconUrl: 'data/icons/table-picnic.svg',
    iconSize: [20, 20],
});

export const doorIcon = L.icon({
    iconUrl: 'data/icons/door.svg',
    iconSize: [40, 40],
});

export const benchIcon = L.icon({
    iconUrl: 'data/icons/bench.svg',
    iconSize: [20, 20],
});

export const wasteIcon = L.icon({
    iconUrl: 'data/icons/trashcan.svg',
    iconSize: [20, 20],
});

export const miscellaneousIcon = L.icon({
    iconUrl: 'data/icons/marker.svg',
    iconSize: [25, 25],
});

export const toiletIcon = L.icon({
    iconUrl: 'data/icons/wc.svg',
    iconSize: [20, 20],
})

export const fitnessIcon = L.icon({
    iconUrl: 'data/icons/fitness.svg',
    iconSize: [20, 20],
})

export const publicTransportIcon = L.icon({
    iconUrl: 'data/icons/bus.svg',
    iconSize: [20, 20],
})

export const sculptureIcon = L.icon({
    iconUrl: 'data/icons/sculpture.svg',
    iconSize: [25, 25],
})

export const mainEntryIcon = L.icon({
    iconUrl: 'data/icons/mainEntry.svg',
    iconSize: [40, 40],
});
