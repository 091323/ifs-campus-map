let state = {}

export const updateState = (name, value) => state[name] = value
export const getState = (name) => {
   return  state[name]
}