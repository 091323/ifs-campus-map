import { getState } from "./state"
import { GEOJSON_DATA, MY_MAP } from "./state/constants"
import {selectLevel} from './layer'

export const createLevels = () => {
    const selectLevelTwoBtn = document.querySelector('#selectLevelTwo')
    const selectLevelOneBtn = document.querySelector('#selectLevelOne')
    const selecteLevelZeroBtn = document.querySelector('#selectLevelZero')
    const selectLevelMinusOneBtn = document.querySelector('#selectLevelMinusOne')

    selectLevelTwoBtn.addEventListener('click', (event) => selectLevelTwo(event.target))
    selectLevelOneBtn.addEventListener('click', (event) => selectLevelOne( event.target))
    selecteLevelZeroBtn.addEventListener('click', (event) => selectLevelZero(event.target))
    selectLevelMinusOneBtn.addEventListener('click', (event) => selectLevelMinusOne(event.target))

    selectLevelZero(selecteLevelZeroBtn)
}

export const selectLevelTwo = (btn) => {
    const myMap = getState(MY_MAP)
    const geojsondata = getState(GEOJSON_DATA)
    selectButton(btn);
    selectLevel(geojsondata['levelTwo'], geojsondata['levelCoridorTwo'], myMap);
}

export const selectLevelOne = (btn)=> {
    const myMap = getState(MY_MAP)
    const geojsondata = getState(GEOJSON_DATA)
    selectButton(btn);
    selectLevel(geojsondata['levelOne'], geojsondata['levelCoridorOne'], myMap);
}

export const selectLevelZero = (btn) => {
    const myMap = getState(MY_MAP)
    const geojsondata = getState(GEOJSON_DATA)
    selectButton(btn);
    selectLevel(geojsondata['levelZero'], geojsondata['levelCoridorZero'], myMap);
}

export const selectLevelMinusOne = (btn) => {
    const myMap = getState(MY_MAP)
    const geojsondata = getState(GEOJSON_DATA)
    selectButton(btn);
    selectLevel(geojsondata['levelMinusOne'], geojsondata['levelCoridorMinusOne'], myMap);
}

const selectButton = button => {
    document.querySelector('#selectLevelTwo').className = 'levelButton';
    document.querySelector('#selectLevelOne').className = 'levelButton';
    document.querySelector('#selectLevelZero').className = 'levelButton';
    document.querySelector('#selectLevelMinusOne').className = 'levelButton';

    button.className = 'pressedLevelButton';
}
