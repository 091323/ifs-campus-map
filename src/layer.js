import { formatSidebarInfo, formatKey, formatValue } from './helper.js';
import { selectedFeatureStyle, corridorStyle, poiPropertiesToShow, infoIcon, toiletIcon, miscellaneousIcon, wasteIcon, benchIcon, sculptureIcon, mainEntryIcon, fitnessIcon, picnicTableIcon, publicTransportIcon, doorIcon, hoverFeatureStyle } from './const.js';
import { getState } from './state';
import { GEOJSON_DATA, MY_MAP } from './state/constants.js';

let localState = {
    layerGroup:  new L.LayerGroup(),
    selectedFeatureLayer:  L.tileLayer(''),
    selectedLevelRoomLayer:  L.tileLayer(''),
    selectedLevelCoridorLayer:  L.tileLayer(''),
    poiLayer:  L.tileLayer(''),
    doorsLayer:  L.tileLayer(''),
    selectedHoverLayer: L.tileLayer('')
}


export const createLayers = () => {
    const myMap = getState(MY_MAP)
    const geoJsonData = getState(GEOJSON_DATA)
    localState.selectedHoverLayer.addTo(myMap)
    localState.layerGroup.addTo(myMap)
    localState.poiLayer = loadPoiLayer(geoJsonData);
}

const loadPoiLayer = geojsondata => {
    return L.geoJSON(geojsondata['POI'], { 
        pointToLayer: function(feature, latlng){
            if(feature['properties']['amenity'] === 'table'){
                return new L.marker(latlng, {icon: picnicTableIcon, interactive: false});
            } 
            if (feature['properties']['information'] !== undefined) {
                return new L.marker(latlng, {icon: infoIcon, interactive: false});
            } 
            if (feature['properties']['amenity'] === 'bench') {
                return new L.marker(latlng, {icon: benchIcon, interactive: false});
            } 
            if (feature['properties']['amenity'] === 'waste_basket') {
                return new L.marker(latlng, {icon: wasteIcon, interactive: false});
            } 
            if (feature['properties']['leisure'] === 'picnic_table') {
                return new L.marker(latlng, {icon: picnicTableIcon, interactive: false});
            } 
            if (feature['properties']['amenity'] === 'toilets') {
                return new L.marker(latlng, {icon: toiletIcon, interactive: false});
            } 
            if(feature['properties']['fitness_station'] !== undefined || feature['properties']['sport'] === 'fitness' ) {
                return new L.marker(latlng, {icon: fitnessIcon, interactive: false});
            } 
            if(feature['properties']['public_transport'] !== undefined) {
                return new L.marker(latlng, {icon: publicTransportIcon});
            } 
            if(feature['properties']['tourism'] === 'artwork') {
                return new L.marker(latlng, {icon: sculptureIcon, interactive: false});
            }
            if (feature['properties']['entrance'] === 'main') {
                return new L.marker(latlng, {icon: mainEntryIcon, interactive: false});
            } 
            else {
                return new L.marker(latlng, {icon: miscellaneousIcon});
            }
        },

        onEachFeature: function(feature, layer) {
            layer.on('click', function (e) {
                const info = document.getElementById('info');
                info.innerHTML = '';
                const featureProperties = feature['properties'];
                for (let id in featureProperties) {
                    if (poiPropertiesToShow.includes(id)) {
                        info.innerHTML += '<p>' + formatKey(id) + formatValue(featureProperties[id])  + '</p>';
                    }
                }
                if (info.innerHTML !== '' && feature['properties']['public_transport'] === undefined) {
                    if (feature['properties']['level'] !== undefined) {
                        info.innerHTML += '<p>'+ feature['properties']['level'] + '</p>';
                    } else {
                        info.innerHTML += '<p>Stockwerk 0</p>';
                    }
                }
            });
        },
        
    });
}

export const selectLevel = (results, rooms, myMap) => {
    clearSelectedFeature()
    const info = document.getElementById('info');
    info.innerHTML = ""
    localState.layerGroup.removeLayer(localState.selectedLevelRoomLayer);
    localState.layerGroup.removeLayer(localState.selectedLevelCoridorLayer);

    localState.selectedLevelRoomLayer = L.geoJSON(results, {
        pointToLayer: function(feature, latlng){
            return null;
        },
        onEachFeature: function(feature, layer) {                 
            layer.on('click', function (e) {
                document.getElementById('listbox').innerHTML = '';

                selectGeojson(feature);
                
                L.DomEvent.stop(e);
                formatSidebarInfo(feature, document.getElementById('info'));
            });
            layer.on('mouseover', () => {
                drawRoomBorder(feature, hoverFeatureStyle)    
                layer.bindPopup(createPopup(feature), {closeButton: false}).openPopup()
            })
            layer.on('mouseout', () => {
                layer.closePopup()
                localState.layerGroup.removeLayer(localState.selectedHoverLayer)
            })
        }
    });

    

    localState.selectedLevelCoridorLayer = L.geoJSON(rooms, {
        pointToLayer: function(feature, latlng){
            return null;
        },
        onEachFeature: function(feature, layer) {                 
            layer.on('click', function (e) {
                document.getElementById('listbox').innerHTML = '';

                selectGeojson(feature);
                
                L.DomEvent.stop(e);
                formatSidebarInfo(feature, document.getElementById('info'));
            });
        }
    });

    localState.selectedLevelCoridorLayer.setStyle(corridorStyle);

    localState.layerGroup.addLayer(localState.selectedLevelCoridorLayer);
    localState.layerGroup.addLayer(localState.selectedLevelRoomLayer);
    
    if(localState.selectedFeatureLayer !== null){
        localState.selectedFeatureLayer.bringToFront();
    }

    if(localState.doorsLayer !== null){
        localState.layerGroup.removeLayer(localState.doorsLayer);
    }
    let doors = [];
    for(let featureId in results){
        if(results[featureId]['geometry']['type'] === 'Point'){
            doors.push(results[featureId]);
        }
    }
    localState.doorsLayer = L.geoJSON(doors, {
        pointToLayer: function(feature, latlng){
            if(feature['properties']['door'] !== undefined && feature['properties']['entrance'] !== 'main'){
                return new L.marker(latlng, {icon: doorIcon});
            }
        },
    });
    updateDoors(myMap);
    
    document.getElementsByClassName('pressedlvlButton').className = 'lvlButton';
    localState.selectedLevelRoomLayer.bringToFront();
}

export const updateDoors = myMap => {
    if (myMap.getZoom() > 20){
        localState.layerGroup.addLayer(localState.doorsLayer);
    } 
    else {
        localState.layerGroup.removeLayer(localState.doorsLayer);
    }
}

export const updatePOI = myMap => {
    if (myMap.getZoom()> 19){
        localState.layerGroup.addLayer(localState.poiLayer);
    } 
    else {
        localState.layerGroup.removeLayer(localState.poiLayer);
    }
}

export const clearSelectedFeature = _ => {
    localState.layerGroup.removeLayer(localState.selectedFeatureLayer);
}

export const selectGeojson = (geojson = {}, featureStyle = selectedFeatureStyle) => {
    clearSelectedFeature();

    localState.selectedFeatureLayer = L.geoJSON(geojson, {
        style: featureStyle,

        onEachFeature: (feature, layer)  => {
            layer.on('click', (e) => {
                L.DomEvent.stop(e);
                document.getElementById('listbox').innerHTML = '';
                formatSidebarInfo(feature, document.getElementById('info'));
            });    
            layer.on('mouseover', () => {
                drawRoomBorder(feature, hoverFeatureStyle)    
                layer.bindPopup(createPopup(feature), {closeButton: false}).openPopup()
            })
            layer.on('mouseout', () => {
                localState.layerGroup.removeLayer(localState.selectedHoverLayer)
                layer.closePopup()
            })

        }
    });
    localState.layerGroup.addLayer(localState.selectedFeatureLayer);
}

const drawRoomBorder = (geojson = {}, featureStyle = selectedFeatureStyle) => {
    const drawedBorderLayer = L.geoJSON(geojson, {
        style: featureStyle,
        interactive: false
    })
    localState.layerGroup.removeLayer(localState.selectedHoverLayer)
    localState.selectedHoverLayer = drawedBorderLayer
    localState.layerGroup.addLayer(drawedBorderLayer)
}

const createPopup = (feature) => feature ? L.popup().setContent(`<div>
                <div class='popup-title'>${feature.properties.name ?? feature.properties.description}</div>
                ${feature.properties.room ? '<div>Type: ' + feature.properties.room + '</div>' : ''}`): null