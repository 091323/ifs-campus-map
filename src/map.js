import { getParameter } from './helper.js';
import { MAP_TILE_SERVER, MY_MAP } from './state/constants.js';
import { getState, updateState } from './state';
import {updateDoors, updatePOI} from './layer'
import {clearSelectedFeature} from './layer'

//local state
let localState = {
    mapTileServer: null,
    tileServerToggle: false
}

export const createMyMap = () => {
    const {myMap, mapTileServer} = setupMap()
    
    updateState(MY_MAP, myMap);
    localState.mapTileServer = mapTileServer

    myMap.on('zoomend' , function (e) {
        updateDoors(myMap);
        updatePOI(myMap);
    });
    
    myMap.on('click', function (e) {
        info.innerHTML = '';
        listbox.innerHTML = '';
        clearSelectedFeature();
    });
    
    myMap.on('moveend', function (e) {
        let address = window.location.href.split('?')[0];
        const center = myMap.getCenter();
        const zoom = myMap.getZoom();
        address += `?lat=${center.lat.toFixed(7)}&lon=${center.lng.toFixed(7)}&zoom=${zoom}`;
    
        window.history.pushState('page2', 'Title', address);
    });
}

const setupMap = () => {
    const myMap = loadMap() 
    const mapTileServer = setMapTileServer()

    myMap.attributionControl.setPrefix('<a href="https://www.ifsoftware.ch/index.php?id=5678">Kontakt</a> | © <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | <a href="https://www.openstreetmap.org/fixthemap?lat=46.998051&lon=8.482818&zoom=18">Behebe einen Fehler</a>');
    mapTileServer.addTo(myMap);
    myMap.removeControl(myMap.zoomControl);
    myMap.options.minZoom = 17;
    return {myMap, mapTileServer}
}

const loadMap = _ => {
    const lat = getParameter("lat");
    const lon = getParameter("lon");
    const zoom = getParameter("zoom");

    if(lat && lon && zoom){
        return L.map('mapid').setView([lat, lon], zoom);
    } else {
        return L.map('mapid').setView([47.223345, 8.817153], 18);
    }
}

const setMapTileServer = _ => {
    return L.tileLayer('https://api.maptiler.com/maps/basic/{z}/{x}/{y}.png?key=NKLz2KbROleVyHg340qT', {
        attributionControl: false,
        maxZoom: 22,
        minZoom: 14,
        maxNativeZoom: 20,
        tileSize: 512,
        zoomOffset: -1,
        zoomControl: false
    });
}

export const toggleTileServer = _ => {
    localState.tileServerToggle = !localState.tileServerToggle
    const mapTileServer = localState.mapTileServer
    const tileServerToggle = localState.tileServerToggle

    if(tileServerToggle){
        mapTileServer.setUrl('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}')
        document.getElementById("changeMap").style.backgroundImage = "url(data/icons/satellite_on.svg)";
        document.getElementById("changeMap").title = "Satellitenbild aus"
    } else {
        mapTileServer.setUrl('https://api.maptiler.com/maps/basic/{z}/{x}/{y}.png?key=NKLz2KbROleVyHg340qT')
        document.getElementById("changeMap").style.backgroundImage = "url(data/icons/satellite_off.svg)";
        document.getElementById("changeMap").title = "Satellitenbild an"
    }
}
