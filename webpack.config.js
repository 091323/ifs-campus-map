const path = require('path'); // importing some packages. Be aware we are here in the nodejs environment

const config = {
  entry: './src/index.js', // the entry file, which we will compile
  output: {
    filename: 'main.js', // name of the compiled JS code file
    path: path.resolve(__dirname, 'dist'), // destination folder after build is done
  },
  devServer: {
      contentBase: './dist', // base for hot module reloading
      // hot: true // we enable hot module reloading
  },

};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map';
  }

  return config
};